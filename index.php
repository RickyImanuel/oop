<?php

    require_once 'Animal.php';
    require_once 'Ape.php';
    require_once 'Frog.php';

    $sheep = new Animal("shaun");
    echo "SOAL 1<br>";
    echo $sheep->name . "<br>"; // "shaun"
    echo $sheep->legs . "<br>"; // 2
    echo $sheep->cold_blooded . "<br>"; // false

    echo "<br>SOAL 2<br>";
    $sungokong = new Ape("kera sakti");
    echo $sungokong->name . "<br>";
    echo $sungokong->legs . "<br>"; 
    $sungokong->yell(); // "Auooo"
    echo "<Br>";
    $kodok = new Frog("buduk");
    echo $kodok->name . "<br>"; 
    echo $kodok->legs . "<br>"; 
    $kodok->jump(); // "hop hop"
    
?>

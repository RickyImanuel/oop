<?php

class Frog extends Animal{
    function jump(){
        echo "hop hop<br>";
    }
    //override
    public function __construct($name){
        $this->name = $name;
        $this->legs = 4;
        return $this;
    }
}
   
?>